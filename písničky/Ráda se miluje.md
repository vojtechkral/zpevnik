# Ráda se miluje
## Karel Plíhal

$ 3/4

> `Hm`Ráda se miluje, `A`ráda `D`jí, `G`ráda si `F#m`jenom tak `Hm`zpívá,
vrabci se na plotě `A`háda`D`jí, `G`kolik že `F#m`času jí `Hm`zbývá.

1. `G`Než vítr dostrká `D`k útesu `G`tu její legrační `D`bá`F#`rku
a `Hm`Pámbu si ve svým `A`note`D`su `G`udělá `F#m`jen další `Hm`čárku.
ℛ.

2. Psáno je v nebeské režii, a to hned na první stránce,
že naše duše nás přežijí v jinačí tělesný schránce.
ℛ.

3. Úplně na konci paseky, tam, kde se ozvěna tříští,
sedí šnek ve snacku pro šneky - snad její podoba příští.
ℛ.

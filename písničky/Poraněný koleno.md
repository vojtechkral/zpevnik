# Poraněný koleno
## Pacifik

1. `Em`Poraněný koleno to `D`místo jméno má,
`Em`krví zpitá země, co `D`tance duchů zná,
`Em`bídu psanců štvanejch a z `D`kulovnice strach,
`C`země, která skrývá zlatej `H7`prach.

2. Dýmky míru zhasly a zoufalství jde tam
na pahorek malý, kde z hanby stojí chrám,
z hanby bledejch tváří, co prej dovedou žít,
`C`s písmem svatým `H7`přes mrtvoly `Em`jít.

> `Em`Až z vetrnejch plání `C`zazní mocný `G`hlas,
zazní `Em`hlas, zazní `G`hlas touhy `Em`mý,
Siouxové táhnou z `C`rezervací `G`zas,
táhnou `Em`zas, táhnou `G`zas, stateč`Em`ní.

3. Poraněný koleno je místo prokletí,
Siouxové táhnou jak tenkrát před lety,
pošlapaný právo kdo v zemi otců má,
tomu slova jsou tak zbytečná.

4. Poraněný koleno, at hoří Kristův kříž,
dětem týhle země at slunce vyjde blíž,
láska kvete krajem a písně znějí dál
tam, kde hrdý Sioux věrne stál.
ℛ

5. Černý hory v dálce prý otvírají den,
tam z jeskyně větru k nám Mesiáš jde ven,
voják hlavu sklopí a k nohám hodí zbraň,
pochopí, kdo splatil krutou daň.

6. Třista bojovníku tak, jako jeden muž,
neproklál jim srdce zlý nenávisti nůž,
pro kousek svý země svůj život půjdou dát,
i jejich děti chtěj' si taky hrát.
ℛ

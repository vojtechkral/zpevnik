# Kluziště
## Karel Plíhal

1. `C`Strejček `Em`kovář `Am7`chytil `C`kleště, 
`Fmaj7`uštíp' z `C`noční `Fmaj7`oblo`G`hy
jednu malou kapku deště, ta mu spadla pod nohy,
nejdřív ale chytil slinu, tak šáh' kamsi pro pivo,
pak přitáhl kovadlinu a obrovský kladivo.

> Zatím `C`tři bílé `Em`vrány pě`Am7`kně za se`C`bou
kolem jd`Fmaj7`ou, někam jd`C`ou, do ryt`D7`mu se kývaj`G`í,
tyhle tř`C`i bílé vr`Em`ány pěk`Am7`ně za seb`C`ou
kolem jd`Fmaj7`ou, někam jd`C`ou, nedojd`Fmaj7`ou, nedojd`C/G`ou.

2. Vydal z hrdla mocný pokřik ztichlým letním večerem,
pak tu kapku všude rozstřík' jedním mocným úderem,
celej svět byl náhle v kapce a vysoko nad námi
na obrovské mucholapce visí nebe s hvězdami.
ℛ

3. Zpod víček mi vytrysk' pramen na zmačkané polštáře,
kdosi mě vzal kolem ramen a políbil na tváře,
kdesi v dálce rozmazaně strejda kovář odchází,
do kalhot si čistí dlaně umazané od sazí. 
ℛ

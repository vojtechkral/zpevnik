# Omnia Vincit Amor
## Klíč

^7

1. `Dm`Šel pocestný kol `C`hospodských `Dm`zdí,
`F`přisedl k nám a `C`lokálem `F`zní
`Gm`pozdrav jak svaté `Dm`přikázá`C`ní:
`Dm`Omnia `C`vincit `Dm`Amor.

2. Hej, šenkýři, dej plný džbán,
ať chasa ví, kdo k stolu je zván,
se mnou ať zpívá, kdo za své vzal
Omnia vincit Amor.

> Zlaťák `F`pálí, `C`nesleví `Dm`nic,
štěstí v `F`lásce `C`znamená `F`víc,
všechny `Gm`pány `F`ať vezme `C`ďas! `A7`
`Dm`Omnia `C`vincit `Dm`Amor

3. Já viděl zemi válkou se chvět,
musel se bít a nenávidět,
v plamenech pálit prosby a pláč
Omnia vincit Amor.

4. Zlý trubky troubí, vítězí zášť,
nad lidskou láskou roztáhli plášť,
vtom kdosi krví napsal ten vzkaz:
Omnia vincit Amor.
ℛ

5. Já prošel každou z nejdelších cest,
všude se ptal, co značí ta zvěst,
až řekl moudrý: "Pochopíš sám,
všechno přemáhá láska."
ℛ

6. Teď s novou vírou obcházím svět,
má hlava zšedla pod tíhou let,
každého zdravím tou větou všech vět:
Omnia vincit Amor.
Omnia vincit Amor...

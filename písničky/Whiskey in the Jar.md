# Whiskey in the Jar

1. As `C`I was a goin' over the `Am`far famed Kerry mountains
I `F`met with Captain Farrell and his `C`money he was counting
I first produced my pistol and I `Am`then produced my rapier
Saying `F`"Stand and deliver, for `C`you are a bold deceiver!"

> Mush-a `G`ring dum-a do dum-a da
`C`Whack for me daddy-o
`F`Whack for me daddy-o
There's `C`whiskey `G`in the `C`jar

2. I counted out his money and it made a pretty penny
I put it in me pocket and I took it home to Jenny
She sighed and she swore that she never would deceive me
But the devil take the women for they never can be easy
Mush-a ring dum-a do dum-a da ...

3. I went up to my chamber, all for to take a slumber
I dreamt of gold and jewels and for sure 't was no wonder
But Jenny drew me charges and she filled them up with water
Then sent for captain Farrell to be ready for the slaughter
Mush-a ring dum-a do dum-a da ...

4. 'Twas was early in the morning, just before I rose to travel
Up comes a band of footmen and likewise captain Farrell
I first produced me pistol for she stole away me rapier
I couldn't shoot the water, so a prisoner I was taken
Mush-a ring dum-a do dum-a da ...

5. Now there's some take delight in the carriages a-rollin'
And others take delight in the hurling and the bowling
But I take delight in the juice of the barley
And courting pretty fair maids in the morning bright and early
Mush-a ring dum-a do dum-a da ...

6. If anyone can aid me 't is my brother in the army
If I can find his station in Cork or in Killarney
If he'll go with me, we'll go rovin' through Killkenny
I'm sure he'll treat me better than my own a-sporting Jenny
Mush-a ring dum-a do dum-a da ...

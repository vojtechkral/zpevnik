# Pohár a Kalich
## Klíč

^3

1. `F#m`Zvedněte poháry, `E`pánové, už jen `A`poslední `E`přípitek `F#m`zbývá,
pohleďte, nad polem `E`Špitálským `A`vrcholek `E`roubení `A`skrývá,

za chvíli `E`zbyde tam popel a `F#m`tráva,
nás čeká `E`vítězství, bohatství, `F#m`sláva.

2. Ryšavý panovník jen se smál, sruby prý dobře se boří,
palcem k zemi ukázal, ať další hranice hoří,
[: ta malá země už nemá co získat,
teď bude tancovat, jak budem pískat. :]

> `Hm`Náhle se pozvedl `E`vítr a mocně `A`vál,
`Hm`odněkud přinesl `E`nápěv, sám si ho `C#7`hrál ...

3. Zvedněte poháry,`E` pánové, večer z `F#m`kalichu `E`budeme `F#m`pít.
Nad sruby korouhev zavlála,
to však neznačí, že je tam sláva,
všem věrným pokaždé nesmírnou sílu a jednotu dává,
ve znaku kalich, v kalichu víra,
jen pravda vítězí, v pravdě je síla.

4. 'Modli se, pracuj a poslouchej,' kázali po celá léta,
Mistr Jan cestu ukázal proti všem úkladům světa,
/: být rovný s rovnými, muž jako žena,
dávat všem věcem ta pravá jména. :/

5. Do ticha zazněla přísaha - ani krok z tohoto místa,
se zbraní každý vyčkává, dobře ví, co se chystá,
nad nimi stojí muž, přes oko páska,
slyšet je dunění a dřevo praská.
ℛ

Kdož jsú boží bojovníci a zákona jeho,
prostež od Boha pomoci a úfajte v něho ...

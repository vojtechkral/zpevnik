# The Sound of Silence
## Simon & Garfunkel

1. `Dm`Hello darkness my old `C`friend
I've come to talk with you `Dm`again
because a `F`vision softly `B`cre`F`eping
left it seeds while I was `B`slee`F`ping
and the `B`vision that was planted in my `F`brain
still remains`Dm` `F`within the `C`sound of `Dm`silence.

2. In restless dreams I walked alone
narrow streets of cobble-stone
neath the halo of a street lamp
I turned my collar to the cold and damp
when my eyes were stabbed by the flash of a neon light
that spilt the night and touched the sound of silence.

3. And in the naked light I saw
ten thousand people maybe more
people talking without speaking
people hearing without listening
people writing songs that voices never share
and no one dare disturb the sound of silence.

4. "Fools!" said I "you do not know
silence like a cancer grows
hear my words that I might teach you
take my armsthat I might reach you."
But my words like silent raindrops fell
and echoed in the wells of silence.

5. And the people bowed and prayed
to the neon god they made
and the sign flashed out its warning
in the words that it was forming and the sign said:
"The words of the prophets are written on the subway walls
and tenament halls" and whisper'd in the sound of silence.

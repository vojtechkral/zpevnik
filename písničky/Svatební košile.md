# Svatební košile
## Spirituál kvintet


1. K čertu `D`s tím, že bych `A`si měl zejtra `D`brát
Tu `D`košili, `E`co nemám `A`rád
`D`Že budu `G`milý `D`prsten navlí`F#m/A`kat
Je `G`zbytečný se `A7`kamarádi `D`bát

2. K čertu s tím, že bych se měl sklenky vzdát,
Když duši mou může dál hřát
Ta moje píseň, že jsem věčně mlád
Co zpívat budu, dokud budu stát

> Za `D`vína `A`korbel `D`veršů `Hm`pár dám
`G`Dámě, `A`kterou `D`nesmím zítra `F#m/A`znát
Tu `G`dnešní noc mám `A7`nade všechny `D`rád

3. K čertu s tím, že se bude divný zdát
Až od rána budu se smát
Na milou mou, co chce se tolik vdát
A co mi sotva bude doušek přát

4. K čertu s tím, ať si milá nechá zdát,
Že ráno dá zvony rozhoupat
Ať zvoní zvony třeba tisíckrát
Já za horama budu vzpomínat
ℛ.

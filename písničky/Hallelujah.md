# Hallelujah
## Leonard Cohen

1. (Now) I've `G`heard there was a `Em`secret chord
That `G`David played, and it `Em`pleased the Lord
But `C`you don't really `D`care for music, `G`do you? `D`
It `G`goes like this
The `C`fourth, the `D`fifth
The `Em`minor fall, the `C`major lift
The `D`baffled king `H`composing Halle`Em`lujah

> Halle`C`lujah, Halle`Em`lujah
Halle`C`lujah, Halle`G`lu-uu`D`uu-u`G`jah

2. Your faith was strong but you needed proof
You saw her bathing on the roof
Her beauty and the moonlight overthrew you
She tied you
To a kitchen chair
She broke your throne, and she cut your hair
And from your lips she drew the Hallelujah

ℛ

3. You say I took the name in vain
I don't even know the name
But if I did, well really, what's it to you?
There's a blaze of light
In every word
It doesn't matter which you heard
The holy or the broken Hallelujah

ℛ

4. I did my best, it wasn't much
I couldn't feel, so I tried to touch
I've told the truth, I didn't come to fool you
And even though
It all went wrong
I'll stand before the Lord of Song
With nothing on my tongue but Hallelujah

ℛ

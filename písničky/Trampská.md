# Trampská
## Marek Eben

1. `Dm`Mlhavým ránem bose jdou, kanady vržou na nohou
a dálka tolik vzdále`G`ná je `Dm`blízká,
město jsi nechal za zády, zajíci dělaj' rošády
a v křoví někdo tiše `G`Vlajku `Dm`píská,
`G`najednou připadáš si ňák príma, svobodnej, a tak,
tak `Dm`ňák, tak `G`ňák, ňák `Dm`tak.

> Pojď `F`dál, s náma se nenudíš, pojď dál, ráno se probudíš,
a vedle sebe máš o šišku `Dm`víc,
pojď `F`dál, pod sebou pevnou zem,
pojď dál, a Číro s Melounem,
a Meky, Miky, Vrt, a dál už `G`nic, dál `Dm`nic.

2. Mlhavý ráno za tratí, u cesty roste kapratí,
sbalíš si deku, spacák, celtu, pytel,
a kdyby ňákej úředník začal ti říkat, co a jak,
sbalíš si deku, spacák, celtu, pytel,
důležitý je to, co jseš, odkud jsi přišel a kam jdeš,
co jseš, kam jdeš, co jseš.
ℛ

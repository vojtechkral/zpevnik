# Dokud se zpívá
## Jarek Nohavica

$ 3/4

1. `C`Z Těšína `Em`vyjíždí `Dm7`vlaky co `F`čtvrthodi`C`nu, `Em` `Dm7` `G`
`C`včera jsem `Em`nespal a `Dm7`ani dnes `F`nespoči`C`nu, `Em` `Dm7` `G`
`F`svatý `G`Medard, můj `C`patron, ťuká `Am`si na čel`G`o,
ale `F`dokud se `G`zpívá, `F`ještě se `G`neumře`C`lo. `Em` `Dm7` `G`

2. Ve stánku koupím si housku a slané tyčky,
srdce mám pro lásku a hlavu pro písničky,
ze školy dobře vím, co by se dělat mělo,
ale dokud se zpívá, ještě se neumřelo.

3. Do alba jízdenek lepím si další jednu,
vyjel jsem před chvílí, konec je v nedohlednu,
za oknem míhá se život jak leporelo,
ale dokud se zpívá, ještě se neumřelo.

4. Stokrát jsem prohloupil a stokrát platil draze,
houpe to, houpe to na housenkové dráze,
i kdyby supi se slítali na mé tělo,
tak dokud se zpívá, ještě se neumřelo.

5. Z Těšína vyjíždí vlaky až na kraj světa,
zvedl jsem telefon a ptám se: "Lidi, jste tam?"
A z veliké dálky do uší mi zaznělo,
že dokud se zpívá, ještě se neumřelo,
že dokud se zpívá, ještě se neumřelo.

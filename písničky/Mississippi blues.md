# Mississippi blues
## Pacifik

1. `Am`Říkali mu Charlie a `Dm`jako malej kluk
`Am`kalhoty si `G`o plot potr`Am`hal,
říkali mu Charlie a `Dm`byl to Toma vnuk,
`Am`na plácku rád `G`košíkovou `Am`hrál,
`C`křídou kreslil po ohradách `F`plány dětskejch snů,
`Dm`až mu jednou ze tmy řekli: `E`konec je tvejch dnů,
`Am`někdo střelil zezadu a `Dm`vrub do pažby vryl,
nikdo `Am`neplakal a `G`nikdo nepro`Am`sil.

> Missi`C`ssippi, Missi`Am`ssippi, `F`černý tělo `G`nese říční `C`proud,
Mississippi, Missi`Am`ssippi, `F`po ní bude `G`jeho duše `C`plout. `Am`

2. Říkali mu Charlie a jako každej kluk
na trubku chtěl ve smokingu hrát,
v kapse nosil kudlu a knoflíkovej pluk,
uměl se i policajtům smát,
odmalička dobře věděl, kam se nesmí jít,
který věci jinejm patří a co sám může mít,
že si do něj někdo střelí jak do hejna hus,
netušil, a teď mu řeka zpívá blues.
ℛ

3. Chlapec jménem Charlie, a jemu patří blues,
ve kterým mu táta sbohem dal,
chlapec jménem Charlie snad ušel cesty kus,
jako slepý na kolejích stál,
nepochopí jeho oči, jak se může stát,
jeden že má ležet v blátě, druhej klidně spát,
jeho blues se naposledy řekou rozletí,
kdo vyléčí rány, smaže prokletí.
ℛ

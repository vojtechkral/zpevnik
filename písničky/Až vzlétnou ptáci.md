# Až vzlétnou ptáci
## Victor Jara / Spirituál kvintet
$ 3/4


1. `D`Na předměstí stával dům, `Hm`malý chlapec si tam hrával.
`D`Drak co vzlétal k oblakům, `Hm`všechna tajná přání znával.
`G`Draci totiž vždycky ví to, co je klukům nejvíc líto
`Hm`když musí jít večer spát.

2. Jako víno dozrává, jako v mořích vlny hasnou,
vzpomínkami zůstává na tu smutnou zemi krásnou,
na rybářské staré sítě, na draka a malé dítě,
které nemá si s kým hrát.

3. Na provázku slunce měl, oblohou se za ním vznášel,
jako vánek šel kam chtěl smutné lampy lidem zhášel,
pohádkovou dýchal vůní, mluvil řečí horských tůní,
průzračnou jak dětský smích.

4. Bílý koník běžel dál osamělou pustou plání,
na dlani sníh dětství tál i když padal bez ustání,
den začíná tichou flétnou chvíli předtím nežli vzlétnou,
hejna ptáků ve větvích

5. Na předměstí stával dům, malý Lučín si tam hrával.
Drak co vzlétal k oblakům, všechna tajná přání znával.
Draci totiž vždycky ví to, co je klukům nejvíc líto
když musí jít večer spát.

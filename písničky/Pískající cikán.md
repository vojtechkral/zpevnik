# Pískající cikán
## Spirituál kvintet

^^ 5

1. `D`Dívka `Em`loudá `F#m`se vini`Em`cí, `D`tam, kde `Em`zídka je `F#m`níz`Em`ká,
`D`tam, kde `Em`stráň končí `F#m`voníc`G`í, tam `D`písnič`G`ku někdo `D`pí`G`s`A7`ká.

> (píská se na melodii sloky)

2. Ohlédne se a, propána, v stínu, kde stojí líska,
švarného vidí cikána, jak leží, písničku píská
ℛ

3. Chvíli tam stojí potichu, písnička si ji získá,
domů jdou spolu za smíchu, je slyšet, cikán jak píská.
ℛ

4. Jenže tatík, jak vidí cikána, pěstí do stolu tříská:
"Ať táhne pryč, vesta odraná, groš nemá, něco ti spíská!"
ℛ

5. Teď smutnou dceru má u vrátek, jen Bůh ví, jak se jí stýská.
"Kéž vrátí se mi zas nazpátek ten, který v dálce si píská."
ℛ

6. Pár šídel honí se po louce, na trávě rosa se blýská,
cikán, rozmarýn v klobouce, jde dál a písničku píská.
ℛ

7. Na závěr zbývá už jenom říct, v čem je ten kousek štístka,
peníze často nejsou nic, (: má víc, kdo po svém :)3x si píská.
ℛ

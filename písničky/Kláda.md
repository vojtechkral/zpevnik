# Kláda
## Hop Trop


1. `Hm`Celý roky prachy jsem si skládal, 
`D`nikdy svýho `A`floka nepro`Hm`pil, 
vod lopaty měl vohnutý záda, 
`D`paty od baráku `A`jsem neodle`Hm`pil, 
`A`nikdo neví, do čeho se `Hm`někdy zamotá, 
tohle `D`já už `A`dávno pocho`Hm`pil. 

2. Taky kdysi vydělat jsem toužil, 
brácha řek' mi, že by se mnou šel, 
tak jsem háky, lana, klíny koupil, 
a sekyru jsme svoji každej doma měl, 
a plány veliký, jak fajn budem se mít, 
nikdo z nás pro holku nebrečel. 

> `G`Duní klá`D`da kory`Em`tem, bacha `Hm`dej, `A`hej, bacha `Hm`dej! 
S `G`tou si, `D`bráško, nety`Em`kej, nety`F#`kej ! 

3. Dřevo dostat k pile, kde ho koupí, 
není těžký, vždyť jsme fikaný, 
ten rok bylo jaro ale skoupý, 
a teď jsme na dně my i vory svázaný, 
a k tomu můžem říct jen, že nemáme nic, 
jen kus práce nedodělaný.  
ℛ.  

# Louisiana
## Hop Trop


1. Ten, `Em`kdo by jednou chtěl bejt vopravdickej chlap
a na `G`šífu křížit `D`svět ho nele`Em`ká,
teď příležitost má a stačí, aby se jí drap',
ať `G`na tu chvíli `D`dlouho neče`Em`ká.

> `Em`Louisi`G`a`D`na, Louisi`G`a`D`na
z`G`ná už `D`dálky modravý,`Em`
`G`bí`D`lá Louis`G`ia`D`na, jako `G`víra pevná `D`loď,
podepiš a s náma `Em`pojď,
taky hned si z bečky `D`nahni na zdra`Em`ví.

2. Jó, tady každej z nás má ruku k ruce blíž,
když to musí bejt, i do vohně ji dá,
proti nám je pracháč i kostelní myš,
nám stačí dejchat volně akorát.
ℛ.

3. Až budem někde dál, kde není vidět zem,
dvě hnáty křížem vzhůru vyletí,
zas bude Černej Jack smát se nad mořem,
co je hrobem jeho obětí.
ℛ.

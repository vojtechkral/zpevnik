# Goralé
## selzská lidová


1. `D`Za lasami `G`za górami `A`za dolina`D`mi
pobili się `G`dwo górale `A`ciupaga`D`mi

[: `D`Ej góra`G`le `A`nie bijci `D`się
ma góralka `G`dwa warkocze
`A`podzielicie `D`się :]

2. Za lasami za górami za dolinami
pobili się dwo górale ciupagami
[: Ej górale nie bijci się
ma góralka dvoje oczu podzielicie się :]

3. Za lasami za górami za dolinami
pobili się dwo górale ciupagami
[: Ej górale nie bijci się
ma góralka vjelkie serce podzielicie się :]

4. Za lasami za górami za dolinami
pobili się dwo górale ciupagami
[: Ej górale nie bijci się
ma góralka z przedu z tyłu podzielicie się :]

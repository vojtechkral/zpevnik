# Ďáblovo stádo
## ‘Honec krav’
## ‘Nebeští jezdci’

^^5

1. Po `Am`zasmušilé pustině jel `C`starý honec krav,
den `Am`temný byl a ševelil dech `C`větru v stéblech trav,
tu `Am`honec k nebi pohleděl a v `C`hrůze zůstal `Am`stát,
když z `F`rozervaných oblaků viděl stá`Am`do krav se hnát.

> `Am`Jipija `C`jéj, jipija `Am`jou, to `F`přízraky táhnou t`Am`mou.

2. Ten skot měl rohy z ocele a oči krvavé
a na bocích mu plápolaly cejchy řeřavé,
a oblohou se neslo jejich kopyt dunění
a za ním jeli honáci až k smrti znavení.
ℛ

3. Ti muži byli zsinalí a kalný měli zrak
a marně stádo stíhali, jak mračno stíhá mrak,
proudy potu máčely jim tvář i košili
a oblohou se nesl jejich jekot kvílivý.
ℛ

4. Tu jeden honec zavolal a pravil "Pozor dej,
svou duši hříchu uvaruj a ďáblu odpírej,
bys' nemusel pak po smrti se věky věků štvát
a nekonečnou oblohou to stádo s námi hnát."
ℛ

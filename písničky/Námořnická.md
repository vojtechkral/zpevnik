# Námořnická
## Karel Plíhal

$ 3/4

1. `D`Maličký námořník v `G`krabičce od mýdla
`D`vydal se `A7`napříč va`D`nou,
bez mapy, buzoly, `G`vesel a kormidla
`D`pluje za `A7`krásnou Ja`D`nou.

> `F#m7`Za modrým `Hm7`obzorem `F#m7`dva mysy `Hm7`naděje
`E`lákají odvážné `A7`kluky, `D`snad právě na něho
`G`štěstí se usměje, `D`cíl má už `A7`na dosah `D`ruky.

2. Maličký námořník v krabičce od mýdla
zpocený tričko si svlíká, moře je neklidný,
Jana je nastydlá, kašle a loďkou to smýká.
ℛ.

3. Maličký námořník s vlnami zápasí,
polyká mýdlovou pěnu, loďka se potápí,
v takovémto počasí je těžké dobývat ženu.

> Za modrým obzorem dva mysy naděje
čekají na další kluky,
maličký námořník i študák z koleje
mají cíl na dosah ruky.

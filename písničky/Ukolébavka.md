# Ukolébavka
## Nezmaři

$ 3/4

1. `Am`Máš už `C`spát, `G`klidně `Dm`spát,
`C`sen ti `E7`vrátka ote`Am`vírá,
`Am`máš už `C`spát, `G`  klidně `Dm`spát,
`C`křídlem `E7`noc únavu `Am`stírá.

> Už `C`ovečky jdou `Fmaj7`tajemnou tmou,
`C`flétna `E7`jim tichounce `Am`zpívá,
máš už `C`spát, `G`klidně `Dm`spát,
`C`sen ti `E7`vrátka ote`Am`vírá.

2. Vím, právě vyplouváš na jezero snů,
mírný vánek loďku hýčká,
snad v dálce uvidíš křišťálový klíč,
tím ti den odemkne víčka.
ℛ.

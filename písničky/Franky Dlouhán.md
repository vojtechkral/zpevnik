# Franky Dlouhán
## Nedvědi

1. Kolik je `C`smutného, když `F`mraky černé `C`jdou
lidem nad hla`G`vou,`F`  smutnou dála`C`vou,
já `C`slyšel příběh, který `F`velkou pravdu `C`měl,
za čas odle`G`těl,`F`  každý zapo`C`mněl.

> Měl kapsu `G`prázdnou Franky Dlouhán,
po Státech `F`toulal se jen `C`sám,
a že byl `F`veselej, tak `C`každej měl ho `G`rád.
tam ruce k`F`  dílu mlčky přiloží a `C`zase jede `Am`dál,
a `F`každej, kdo s ním `G`chvilku byl, tak `F`dlouho `G`se pak `C`smál.

2. Tam, kde byl pláč, tam Franky hezkou píseň měl,
slzy neměl rád, chtěl se jenom smát.
A když pak večer ranče tiše usínaj,
Frankův zpěv jde dál, nocí s písní dál.
ℛ

3. Tak Frankieho vám jednou našli, přestal žít,
jeho srdce spí, tiše, smutně spí.
Bůhví jak, za co, tenhle smíšek konec měl,
farář píseň pěl, umíráček zněl.
ℛ

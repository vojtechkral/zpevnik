# Panenka
## Poutníci

$ 3/4
^7

1. Co `G`skrýváš za `C`víčky a `G`plameny `C`svíčky,
snad `G`houf bílých holubic nebo jen `D7`žal,
tak `C`odplul ten `G`prvý den `C`smáčený `G`krví,
ani pouťovou panenku `D`nezane`G`chal.

> `G`Otevři `C`oči, ty `G`uspěcha`D`ná `C`dámo `G`uplaka`D`ná,
`C`otevři `G`oči, ta `C`hloupá noc `G`končí a mír je `D`mezi ná`G`ma.

2. Už si oblékni šaty i řetízek zlatý
a umyj se, půjdeme na karneval,
a na bílou kůži ti napíšu tuší,
že dámou jsi byla a zůstáváš dál.
ℛ

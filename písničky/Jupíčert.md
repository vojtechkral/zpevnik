# Jupí, čerte
## ‘Zatracenej život’

$ 3/4

1. `G`To bylo v Dakotě `C`po vejpla`G`tě, whisky jsem tam pašo`D7`val,
`G`a že jsem byl sám ja`C`ko kůl v plo`G`tě,
s holkou jsem `D7`tam špáso`G`val.

2. Šel jsem s ní nocí, jak vede stezka, okolo Červených skal,
než jsem jí stačil řict, že je hezká,
zpěněnej býk se k nám hnal.
Povídám...

> `C`Jupí, čerte, `G`jdi radši dál, pak jsem ho za rohy `D7`vzal,
`G`udělal přemet a `C`jako tur `G`řval, do dáli `D7`upalo`G`val.

3. To bylo v Dawsonu tam v saloonu a já jsem zase přebral,
všechny svý prachy jsem měl v talónu,
na život jsem nadával.

4. Zatracenej život, čert by to spral, do nebe jsem se rouhal,
než jsem se u baru vzpamatoval,
Belzebub vedle mě stál.
ℛ

5. Jó, rychle oplácí tenhleten svět, než bys napočítal pět,
Ďáblovým kaňónem musel jsem jet,
když jsem se vracel nazpět.

6. Jak se tak kolíbám, uzdu v pěsti, schylovalo se k dešti,
Belzebub s partou stál vprostřed cesty,
zavětřil jsem neštěstí.
Povídám..

> Jupí, čerte, jdi radši dál, potom mě za nohy vzal,
udělal jsem přemet, jako tur řval,
do dáli upaloval.

# Proklatej vůz
## Greenhorns 

1. `C`Čtyři `G`bytelný `Am`kola `F`má náš `C`proklatej `G`vůz,
tak `C`ještě `F`pár `C`dlouhejch `G`mil `C`zbejvá `F`nám, `C`tam je `G`cíl,
a tak `C`zpívej o `F`San`G`ta `C`Cr`F`uz`C`.

> `F`Polykej whisky a `C`zvířenej prach, `G`nesmí nás `C`porazit `C7`strach,
až `F`přejedem támhleten `C`pískovej plát,
pak `D7`nemusíš se`G`  už rudochů `C`bát.

### Rec.
"Georgi, už jsem celá roztřesená, zastav!"
"Zalez zpátky do vozu, ženo!"

2. Jen tři bytelný kola ... ℛ.
"Tatínku, tatínku, už mám plnej nočníček!"
"Probůh, to není nočníček, to je soudek s prachem!"

3. Jen dvě bytelný kola ... ℛ.
"Synu, vždyť jedeme jak s hnojem!"
"Jó, koho jsem si naložil, toho vezu!"

4. Už jen jediný kolo ... ℛ.
"Georgi, zastav, já se strašně bojím Indiánů!"
"Zatáhni za sebou plachtu, ženo, a mlč!"

5. Už ani jediný kolo nemá náš proklatej vůz ...
ℛ.

# Dirty Old Town
## The Pogues

1. I met my `D`love by the gas works wall
Dreamed a `G`dream by the old `D`canal
I Kissed my `Hm`girl by the factory `D`wall
Dirty old `A`town,`A7`  dirty old `Hm`town

2. Clouds are drifting across the moon
Cats are prowling on their beat
Spring's a girl from the streets at night
Dirty old town, dirty old town

3. I Heard a siren from the docks
Saw a train set the night on fire
I Smelled the spring on the smoky wind
Dirty old town, dirty old town

4. I'm gonna make me a big sharp axe
Shining steel tempered in the fire
I'll chop you down like an old dead tree
Dirty old town, dirty old town

5. I met my love by the gas works wall
Dreamed a dream by the old canal
I kissed my girl by the factory wall
Dirty old town, dirty old town
Dirty old town, dirty old town

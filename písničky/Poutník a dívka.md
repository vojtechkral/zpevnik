# Poutník a dívka
## Spirituál Kvintet


1. `A`Kráčel krajem poutník, šel sám,
`D`kráčel krajem poutník, šel `A`sám,
kráčel krajem poutník, kráčel s`C#`ám `F#m`
tu potkal `H`dívku, nesla dž`H7`bán,
přistoupil k `E4sus`ní a pra`E7`vil:


2. Ráchel, Ráchel, žízeň mě zmáhá, ×3
tak přistup blíže, nehodná, a dej mi pít, a ona:

3. Kdo jsi, kdo jsi, že mi říkáš jménem, ×3
já tě vidím poprvé, odkud mě znáš?

4. Ráchel, Ráchel, znám víc než jméno, ×3
pak se napil, ruku zdvih' a kráčel dál.

5. Ten džbán, ten džbán z nepálené hlíny, ×3
v onu chvíli zazářil kovem ryzím.

6. Kráčel krajem poutník, šel sám, ×3
ač byl chu`H`dý, nepoz`E`nán, přece byl `A`král `D` `A`

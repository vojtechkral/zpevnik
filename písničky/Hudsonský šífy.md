# Hudsonský šífy
## Wabi Daněk


1. Ten, kdo `Am`nezná hukot vody lopat`C`kama vířený
jako `G`já, jako `C`já, kdo hud`Am`sonský slapy nezná
sírou `G`pekla sířený,
ať se na `Am`hudsonský `G`šífy najmout `Am`dá, `G`joho`Am`ho.

2. Ten, kdo nepřekládal uhlí, šíf když na mělčinu vjel,
málo zná, málo zná, ten, kdo neměl tělo ztuhlý
až se nočním chladem chvěl,
ať se na hudsonský šífy najmout dá, johoho.

> A`F`hoj, páru tam `Am`hoď,
ať `G`do pekla se dříve dohra`Am`bem,
`G`joho`Am`ho, `G`joho`Am`ho.

3. Ten, kdo nezná noční zpěvy zarostenejch lodníků
jako já, jako já, ten, kdo cejtí se bejt chlapem,
umí dělat rotyku,
ať se na hudsonský šífy najmout dá, johoho.

4. Ten, kdo má na bradě mlíko, kdo se rumem neopil,
málo zná, málo zná, kdo necejtil hrůzu z vody,
kde se málem utopil,
ať se na hudsonský šífy najmout dá, johoho.
ℛ

5. Kdo má roztrhaný boty, kdo má pořád jenom hlad
jako já, jako já, kdo chce celý noci čuchat
pekelnýho vohně smrad,
ať se na hudsonský šífy najmout dá, johoho.

6. Kdo chce zhebnout třeba zejtra, komu je to všechno fuk,
kdo je sám, jó jako já, kdo má srdce v správným místě,
kdo je prostě príma kluk,
ať se na hudsonský šífy najmout dá, johoho.
ℛ
... `G`joho`A`ho

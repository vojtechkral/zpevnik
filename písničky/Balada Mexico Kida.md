# Balada Mexico Kida
## Waldemar Matuška

$ 3/4

1. `D`V Lincolnu loňskýho `G`roku Jim `D`Stapleton `Hm`farao `Em`hrál,`F#m7`
že `D`přitom se kapánek `G`serval`Gm`, tak `D`šerif mu `Hm`náramky `E7`dal.`Em`
Do `D`Santa Fé když ho `G`vezl, Jim `D`řekl, ať tak nebo `Em`tak, `F#m7`
já `D`natuty vezmu ti `G`roh`Gm`a  a `D`budu zas `Em`volnej jak `D`pták.

2. Ten večer jsme v Johnnyho baru srkali černý kafe a
říkali jsme si, jak asi baví se Jim v Santa Fe.
Najednou náramná rána, rozbila lucernu a
v tu ránu v celičkým báru nastala egyptská tma.

3. Na prahu postava stála a že se hned ochladil vzduch,
tu bylo každýmu jasný, že je to nějakej duch.
A v Santa Fé zrovna v ten moment, říkám jen co o tom vím,
z basy když oknem bral roha, to nakoupil nebohej Jim.
